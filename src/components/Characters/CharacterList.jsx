import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import connect from 'react-redux/es/connect/connect';
import _ from 'lodash'
import Table from '@material-ui/core/Table';
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from '@material-ui/core/TableCell';
import compose from 'recompose/compose';
import { withRouter } from 'react-router-dom';
import { getThumbnailPath, getTruncatedDescription } from "../../libs/characterLib";
import { CustomTableHead, CustomTableBody, CustomTableFooter } from '../common/table/CustomTable';
import CircularSpinning from '../common/spinning/CircularSpinning';
import { getPaging } from '../../selectors';
import { getAllCharacters } from '../../actions/CharacterAction';

class CharacterList extends Component {

  renderTableHead = () => {
    const { classes } = this.props;

    return (
      <CustomTableHead>
        <TableRow>
          <TableCell className={classes.cellHeader}>
            Name
          </TableCell>
          <TableCell className={classes.cellHeader}>
            Discription
          </TableCell>
          <TableCell className={classes.cellHeader}>
            Thumbnail
          </TableCell>
        </TableRow>
      </CustomTableHead>
    )
  }

  renderCharacterRecord = (character) => {
    const { classes = {} } = this.props;
    const { thumbnail } = character;
    const hasThumbnail = !_.isEmpty(thumbnail);
    const thumbnailPath = hasThumbnail && getThumbnailPath(thumbnail.path, thumbnail.extension);
    const description = getTruncatedDescription(character.description);

    return (
      <TableRow key={character.id}>
        <TableCell className={classes.characterName}>{character.name}</TableCell>
        <TableCell className={classes.characterDesc}>{description}</TableCell>
        <TableCell>
          <div className={classes.thumbnail} onClick={() => this.showDetail(character.id)}>
            {hasThumbnail && <img src={thumbnailPath} alt={`${character.name}'s thumbnail`} />}
          </div>
        </TableCell>
      </TableRow>
    )
  }

  handleChangePage = (page) => {
    const { paging: { limit } } = this.props;
    const params = {
      offset: limit * (page - 1),
      limit
    }
    this.props.getAllCharacters(params)
  }

  showDetail = (id) => {
    const { history, } = this.props;
    history.push({
      pathname: '/character/' + id
    });
  }

  render() {
    const { characters, paging } = this.props;

    return (characters
      ? <Table>
        {this.renderTableHead()}

        <CustomTableBody>
          {characters && characters.map(character => this.renderCharacterRecord(character))}
        </CustomTableBody>

        <CustomTableFooter
          paging={paging}
          handleChangePage={this.handleChangePage}
          tableLabel={'Characters'}
        />
      </Table>
      : <CircularSpinning />
    );
  }
}

export const styles = {
  cellHeader: {
    color: '#000',
    fontSize: 12,
    textTransform: 'uppercase',
    fontWeight: 600
  },
  thumbnail: {
    overflow: 'hidden',
    cursor: 'pointer',
    width: 100,
    height: 100,
    '& img': {
      transform: 'scale(1.1, 1.1)',
      transition: 'all .5s cubic-bezier(0, 0, 0.37, 0.97)'
    },
    '&:hover': {
      '& img': {
        transform: 'scale(1, 1)'
      }
    }
  },
  characterName: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#B80000',
    fontSize: 14
  },
  characterDesc: {
    fontSize: 18
  }
}

export default compose(
  withRouter,
  withStyles(styles),
  connect(
    state => ({
      characters: state.characters.characterList && state.characters.characterList.results,
      paging: getPaging(state)
    }),
    {
      getAllCharacters,
    }
  )
)(CharacterList);
