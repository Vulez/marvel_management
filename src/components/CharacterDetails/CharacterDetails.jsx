import React, { Component } from 'react';
import compose from 'recompose/compose';
import { connect } from "react-redux";
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { getProfileImagePath } from "../../libs/characterLib";
import { getCharacter } from "../../actions/CharacterAction";
import { ComicList } from "./";
import CircularSpinning from '../common/spinning/CircularSpinning';

class CharacterDetails extends Component {
  render() {
    const { classes, character } = this.props;
    const { thumbnail, name, description, comics: { results } = {} } = character || {};
    const hasThumbnail = !_.isEmpty(thumbnail);
    const photoPath = hasThumbnail && getProfileImagePath(thumbnail.path, thumbnail.extension);

    return (character
      ? <div className={classes.characterDetails}>
        <Grid container>
          <Grid item xs={6} className={classes.leftBlock}>
            <div className={classes.photoWrapper}>
              {hasThumbnail && <img className={classes.photo} src={photoPath} alt={`profile`} />}
            </div>
          </Grid>
          <Grid item xs={6} className={classes.rightBlock}>
            <div className={classes.rightBlockContent}>
              <div className={classes.name}>{name}</div>
              <div className={classes.description}>{description}</div>
            </div>
          </Grid>
        </Grid>
        <ComicList comics={results} />
      </div>
      : <CircularSpinning />
    );
  }
}

const styles = {
  characterDetails: {
    marginTop: 16
  },
  rightBlock: {
    padding: 40
  },
  leftBlock: {
    padding: 40
  },
  photoWrapper: {
  },
  photo: {
    width: '100%'
  },
  name: {
    fontSize: 40,
    textTransform: 'uppercase',
    letterSpacing: 4,
    fontWeight: 'bold'
  },
  description: {
    marginTop: 32,
    letterSpacing: 1.5,
    lineHeight: 1.5,
  },
  rightBlockContent: {
    marginTop: '50%',
    transform: 'translateY(-50%)',
    padding: '0 40px'
  }
}

export default compose(
  withStyles(styles),
  connect(
    state => ({
      character: state.characters.character
    }),
    { getCharacter })
)(CharacterDetails);
