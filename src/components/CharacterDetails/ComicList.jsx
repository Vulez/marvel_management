import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash'
import Table from '@material-ui/core/Table';
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from '@material-ui/core/TableCell';
import compose from 'recompose/compose';
import { getThumbnailPath } from "../../libs/characterLib";
import { CustomTableHead, CustomTableBody } from '../common/table/CustomTable';
class ComicList extends Component {

  renderTableHead = () => {
    const { classes } = this.props;

    return (
      <CustomTableHead>
        <TableRow>
          <TableCell className={classes.cellHeader}>
            Comic
          </TableCell>
          <TableCell className={classes.cellHeader}>
            Description
          </TableCell>
          <TableCell className={classes.cellHeader}>
            Thumbnail
          </TableCell>
        </TableRow>
      </CustomTableHead>
    )
  }

  renderComicRecord = (comic, id) => {
    const { classes = {} } = this.props;
    const { thumbnail, title, description } = comic;
    const hasThumbnail = !_.isEmpty(thumbnail);
    const thumbnailPath = hasThumbnail && getThumbnailPath(thumbnail.path, thumbnail.extension);

    return (
      <TableRow key={id}>
        <TableCell className={classes.comicName}>{title}</TableCell>
        <TableCell className={classes.comicDesc}>{description}</TableCell>
        <TableCell>
          <div className={classes.thumbnail}>
            {hasThumbnail && <img src={thumbnailPath} alt={`${title}'s thumbnail`}/>}
          </div>
        </TableCell>
      </TableRow>
    )
  }

  render() {
    const { comics = [] } = this.props;

    return (
      <Table>
        {this.renderTableHead()}
        <CustomTableBody>
          { comics && comics.map( (comic, idx) => this.renderComicRecord(comic, idx) ) }
        </CustomTableBody>
      </Table>
    );
  }
}

export const styles  = {
  cellHeader: {
    color: '#000',
    fontSize: 12,
    textTransform: 'uppercase',
    fontWeight: 600
  },
  thumbnail: {
    overflow: 'hidden',
    width: 100,
    height: 100,
    '& img': {
      transform: 'scale(1.1, 1.1)',
      transition: 'all .5s cubic-bezier(0, 0, 0.37, 0.97)'
    },
    '&:hover': {
      '& img': {
        transform: 'scale(1, 1)'
      }
    }
  },
  comicName: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#B80000',
  }
}

export default compose(
  withStyles(styles)
)(ComicList);
