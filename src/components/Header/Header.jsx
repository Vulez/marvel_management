import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import compose from 'recompose/compose';
import Logo from '../../assets/images/logo.png';

class Header extends React.Component {
  render() {
    const { classes, isHomePage } = this.props;

    return (
      <div className={classNames({[classes.headerContainer]: isHomePage})}>
        <div className={classes.header}>
          <div>
            <a href="/"><img className={classes.logo} src={Logo} alt="logo"/></a>
          </div>
          <ul className={classes.navigation}>
            <li className={classes.navItem}><a href="/">Home</a></li>
            <li className={classes.navItem}><a href="/about">About</a></li>
          </ul>
        </div>
      </div>
    )
  }
}

export const styles  = {
  headerContainer: {
    '&::after': {
      content: '""',
      position: 'absolute',
      display: 'block',
      height: 160,
      width: '100%',
      top: 0,
      left: 0,
      background: '-webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, rgba(0,0,0,0.9)),color-stop(100%, rgba(0,0,0,0)))',
      background: '-webkit-linear-gradient(top, rgba(0,0,0,0.9) 0%,rgba(0,0,0,0) 100%)',
      background: 'linear-gradient(to bottom, rgba(0,0,0,0.9) 0%,rgba(0,0,0,0) 100%)',
      pointerEvents: 'none',
      transition: 'height .3s'
    },
    '&:hover': {
      '&::after': {
        height: 300,
      }
    },
    '& $header': {
      position: 'absolute',
    },

    '& $navItem': {
      '& a': {
        color: '#ccc'
      }
    }
  },
  header: {
    display: 'flex',
    marginLeft: 32,
    marginTop: 8
  },
  navigation: {
    zIndex: 1,
    listStyleType: 'none',
    margin: 0,
    padding: 0,
    overflow: 'hidden',
    marginLeft: 16
  },
  navItem: {
    float: 'left',
    padding: '32px 24px',
    '& >a': {
      textDecoration: 'none',
      fontWeight: 'bold',
      color: '#000',
      fontSize: 14,
      letterSpacing: 1.5,
      textTransform: 'uppercase',
    },
    '&:hover': {
      '& >a': {
        color: '#B80000',
      }
    }
  },
  logo: {
    height: 80
  }
}
export default compose(
  withStyles(styles),
)(Header);