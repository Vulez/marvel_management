import React, { Component } from 'react';
import IconButton from "@material-ui/core/IconButton/IconButton";
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

class CustomPaginatination extends Component {
	handleGoToFirstPage = () => {
		this.handleGoToPage(1)
	}
	handleGoToPreviousPage = () => {
		const { paging: { page } } = this.props;
		this.handleGoToPage(page - 1)
	}
	handleGoToNextPage = () => {
		const { paging: { page } } = this.props;
		this.handleGoToPage(page + 1)
	}
	handleGoToLastPage = () => {
		const { paging: { pageCount } } = this.props;
		this.handleGoToPage(pageCount)
	}
	handleGoToPage = (page) => {
		this.props.handleChangePage(page);
	}

	render() {
		const { paging, tableLabel } = this.props;
		const { pageCount, total, count, page, offset } = paging;
		const pageInfo = `Showing ${offset + 1} to ${offset + count} of ${total} ${tableLabel}`

		return (
			<span>
				<span>
					<IconButton onClick={this.handleGoToFirstPage} disabled={page === 1}>
						<FirstPageIcon />
					</IconButton>

					<IconButton onClick={this.handleGoToPreviousPage} disabled={page === 1} aria-label="Previous Page">
						<KeyboardArrowLeft />
					</IconButton>

					<IconButton onClick={this.handleGoToNextPage} disabled={page >= pageCount} aria-label="Next Page">
						<KeyboardArrowRight />
					</IconButton>

					<IconButton onClick={this.handleGoToLastPage} disabled={page >= pageCount} aria-label="Last Page">
						<LastPageIcon />
					</IconButton>
				</span>
				<span>
					<span>{pageInfo}</span>
				</span>
			</span>
		);
	}
}

export default CustomPaginatination;