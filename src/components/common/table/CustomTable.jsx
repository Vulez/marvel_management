import React from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableFooter from '@material-ui/core/TableFooter';
import TableCell from '@material-ui/core/TableCell';
import CustomPagination from './CustomPagination';

const styles = theme => ({
  customHead: {
    '& th': {
      borderBottom: '#DEE3ED 1px solid',
      color: '#000',
      fontSize: 12,
      textTransform: 'uppercase',
      fontWeight: 'bold',
    }
  },
  customBody: {
    '& td': {
      borderBottom: '#F2F4F7 1px solid',
    },
    '& tr:hover': {
      boxShadow: '0 0 20px rgba(0,0,0,.1)'
    }
  },
  customFooter: {
    '& td': {
      textAlign: 'center',
      paddingTop: 16
    },
  }
});

export const CustomTableHead = withStyles(styles)(props => {
  const { className, classes, ...rest } = props;
  return (
    <TableHead className={classNames(classes.customHead, { [props.className]: !!props.className })} {...rest}>
      {props.children}
    </TableHead>
  )
});

export const CustomTableBody = withStyles(styles)(props => {
  const { className, classes, ...rest } = props;
  return (
    <TableBody className={classNames(classes.customBody, { [props.className]: !!props.className })} {...rest}>
      {props.children}
    </TableBody>
  )
});

export const CustomTableFooter = withStyles(styles)(props => {
  const { className, classes, ...rest } = props;
  return (
    <TableFooter className={classNames(classes.customFooter, { [props.className]: !!props.className })}>
      <TableRow>
        <TableCell colSpan={3}>
          <CustomPagination
            {...rest}
          />
        </TableCell>
      </TableRow>
    </TableFooter>
  )
});