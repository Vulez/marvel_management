import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

const styles = ({
  progressContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    '& svg': {
      color: '#B80000'
    }
  }
});

const CircularSpinning = ({ classes }) => (
  <div className={classes.progressContainer}>
    <CircularProgress size={50} />
  </div>
);

export default withStyles(styles)(CircularSpinning);
