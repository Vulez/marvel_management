import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: ['Roboto'].join(',')
  },
  palette: {
    primary: {
      main: '#7cb342',
      contrastText: '#fff'
    },
    secondary: {
      main: '#232836'
    }
  },
  overrides: {
    MuiMenuItem: {
      root: {
        '&$selected': {
          backgroundColor: 'unset'
        }
      }
    }
  }
});
