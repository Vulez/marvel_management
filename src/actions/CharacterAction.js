import * as actionType from '../constants/ActionTypes'
import axios from 'axios';

const path = 'http://gateway.marvel.com/v1/public/characters';
const apiKey = '9b43fd80759b14ef3d89db7cd0870880';

export function getAllCharacters({
  limit = 10,
  offset = 0
} = {}) {
  return dispatch => {
    const uri = `${path}?apikey=${apiKey}&limit=${limit}&offset=${offset}`;
    axios.get(uri).then(
      success => {
        dispatch({
          type: actionType.GET_ALL_CHARACTERS,
          payload: success.data.data
        });
      }, failure => {
        throw failure;
      }
    )
  }
}

export function getCharacter(id) {
  return async dispatch => {
    try {
      const characterResult = await axios.get(path + '/' + id + '?apikey=' + apiKey);
      const characterDetails = characterResult.data.data && characterResult.data.data.results[0];
      const {
        comics: {
          collectionURI
        } = {}
      } = characterDetails;
      const comicsResult = await axios.get(collectionURI + '?apikey=' + apiKey);
      const comics = comicsResult.data.data;
      dispatch({
        type: actionType.GET_CHARACTER,
        payload: {
          ...characterDetails,
          comics
        }
      });
    } catch (failure) {
      throw failure;
    }
  }
}

export function resetCharacter() {
  return dispatch => {
    dispatch({
      type: actionType.RESET_CHARACTER
    });
  }
}