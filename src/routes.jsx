import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Characters from './containers/Characters';
import CharacterDetails from './containers/CharacterDetails';

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Characters
  },
  {
    path: '/character/:id',
    name: 'Character Details',
    component: CharacterDetails
  }
];


const AppRoutes = () => (
  <Switch>
    {routes.map((route, key) => {
        const Component = route.component;
        return <Route
                    exact
                    key={key}
                    path={route.path}
                    render={props => <Component {...props} />}
                />
    })}
  </Switch>
);

export default withRouter(
  connect(state => ({
  }))(AppRoutes)
);
