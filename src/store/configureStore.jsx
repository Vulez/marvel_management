import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import createHistory from 'history/createBrowserHistory';

export const history = createHistory();
export function configureStore(initialState = {}) {
  const middlewares = [thunk, routerMiddleware(history)];
  const enhancer = compose(
    applyMiddleware(...middlewares),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  );
  const store = createStore(reducers, initialState, enhancer);
  if (module.hot) {
    module.hot.accept('../reducers', () => store.replaceReducer(require('./../reducers').default));
  }
  return store;
}
