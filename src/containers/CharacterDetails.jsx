import React, { Component } from 'react';
import compose from 'recompose/compose';
import { connect } from "react-redux";
import { CharacterDetails as Details } from '../components/CharacterDetails';
import { getCharacter, resetCharacter } from "../actions/CharacterAction";

class CharacterDetails extends Component {
  componentDidMount() {
    const { match } = this.props;
    const { params: { id } } = match;

    if (id) {
      const data = this.props.getCharacter(id);
      !data && this.props.history.push('/');
    }
  }

  componentWillUnmount() {
    this.props.resetCharacter()
  }

  render() {
    return (
      <React.Fragment>
        <div className="page-title">CHARACTER DETAILS</div>
        <Details />
      </React.Fragment>
    );
  }
}

export default compose(
  connect(null, { getCharacter, resetCharacter })
)(CharacterDetails);
