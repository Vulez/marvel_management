import React, { Component } from 'react';
import { CharacterList } from '../components/Characters';

class Characters extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="page-title">ALL CHARACTERS</div>
        <CharacterList/>
      </React.Fragment>
    );
  }
}

export default Characters;
