import {
  createSelector
} from 'reselect';

const getCharacterList = state => state.characters.characterList;

export const getPaging = createSelector(
  [getCharacterList], (characterList = {}) => {
    const {
      limit,
      offset,
      total,
      count
    } = characterList;

    return {
      limit,
      total,
      count,
      offset,
      page: offset / limit + 1,
      pageCount: Math.ceil(total / limit)
    }
  }
);