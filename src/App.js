import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Header } from './components/Header';
import Banner from './assets/images/marvel-banner.jpg';
import './App.scss';

class App extends Component {
  isHomePage = () => {
    return this.props.location && this.props.location.pathname === '/';
  }

  render() {
    const { classes } = this.props;
    const isHomePage = this.isHomePage();

    return (
      <div>
        <Header isHomePage={isHomePage}/>
        { isHomePage && 
          <div>
            <img className={classes.bannerImg} src={Banner} alt="marvel-banner"/>      
          </div>
        }
        <main className={classes.pageWrapper}>
          <div>
            {this.props.children}
          </div>
        </main>
      </div>
    );
  }
}

const styles = {
  bannerImg: {
    width: '100%'
  },
  pageWrapper: {
    backgroundColor: '#fff',
    padding: '8px 5% 40px 5%'
  }
}


App.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}

export default compose(
  withRouter,
  withStyles(styles)
)(App);
