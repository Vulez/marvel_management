const variantName = 'standard_medium';
const descriptionLimit = 100;

export const getThumbnailPath = (path, extension) => `${path}/${variantName}.${extension}`;

export const getProfileImagePath = (path, extension) => `${path}.${extension}`;

export const getTruncatedDescription = (description) => description && description.length > descriptionLimit 
  ? `${description.substring(0, descriptionLimit)}...` 
  : description;