import * as actionType from "../constants/ActionTypes";
const characterInitialState = {
  characterList: {
    offset: 0,
    limit: 20
  }
};

export default function keycloakReducer(state = characterInitialState, action) {
  switch (action.type) {
    case actionType.GET_ALL_CHARACTERS:
      const characterList = action.payload || {};
      return {
        ...state,
        characterList,
      };
    case actionType.GET_CHARACTER:
      const character = action.payload || {};
      return {
        ...state,
        character
      };
    case actionType.RESET_CHARACTER:
      return {
        ...state,
        character: null
      }
    default:
      return state;
  }
}